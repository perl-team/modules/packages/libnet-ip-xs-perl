Source: libnet-ip-xs-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ondřej Surý <ondrej@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libio-capture-perl <!nocheck>,
               libtie-simple-perl <!nocheck>,
               perl-xs-dev,
               perl:native
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnet-ip-xs-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnet-ip-xs-perl.git
Homepage: https://metacpan.org/release/Net-IP-XS
Rules-Requires-Root: no

Package: libnet-ip-xs-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libio-capture-perl,
         libtie-simple-perl
Description: Perl extension for manipulating IPv4/IPv6 addresses (XS)
 Net::IP::XS is a XS (C) implementation of Net::IP (libnet-ip-perl in
 Debian) that provides functions to deal with IPv4/IPv6 addresses.
 .
 The module can be used as a class, allowing the user to instantiate
 IP objects, which can be single IP addresses, prefixes, or ranges of
 addresses.  There is also a procedural way of accessing most of the
 functions. Most subroutines can take either IPv4 or IPv6 addresses
 transparently.
